local S = minetest.get_translator("server_manager")

local DELAY = 600
local broadcast_index = 1
local messages = {
  "[Server] " .. minetest.colorize("#eea160", S("Got some ideas about how to improve the server? Type /mail in chat to open the mail dialogue and send your suggestions to the username: Server")),
  "[Server] " .. minetest.colorize("#eea160", S("Why did we choose Minetest (which is an engine) instead of Minecraft (which is a videogame)? Because it's free software!")),
  "[Server] " .. minetest.colorize("#eea160", S("Do you want to develop your own minigame? Zughy and Freinds made a library to help you out: search for 'arena_lib' on GitLab and... maybe one day we'll also feature it here!")),
  "[Server] " .. minetest.colorize("#eea160", S("Thank Zughy and Freinds for making minigames possible on minetest! Offer them a coffee on liberapay.com/EticaDigitale")),
  -- "[Server] " .. minetest.colorize("#eea160", S("Playing while in a call is way more fun than alone; and we have a Mumble server for that! Join us at 185.242.180.132")),
  "[Server] " .. minetest.colorize("#eea160", S("This server is hosted by Skizzato and sponsored by IRCnow. Visit the castle to learn more about IRCnow! ircnow.org")),
  "[Server] " .. minetest.colorize("#eea160", S("Connect with us! IRC: irc.ircnow.org #minigames   Matrix: #minigames_galore:matrix.org   Code: gitlab.com/arcadeemulationserver")),
  "[Server] " .. minetest.colorize("#eea160", S("We accept submissions for new arenas: they must be complete, well decorated, well-designed, and in-escapable. Post a link to you git-hosted .mts on matrix or message MisterE on IRC and we will consider it!")),
}


local function main()

    broadcast_index = broadcast_index +1

    if broadcast_index > #messages then
      broadcast_index = 1
    end

    minetest.chat_send_all(messages[broadcast_index])
    minetest.after(DELAY, main)

end

minetest.after(DELAY, main)
