local exceptions = {
  "MisterA",
  "_Zaizen_",
  "Zughy",
  "Giov4",
  "debiankaios_admin",
  "SonoMichele",
  "jrmu"
}

minetest.register_can_bypass_userlimit(function(name, ip)
  return table.indexof(exceptions, name) > 0
end)
