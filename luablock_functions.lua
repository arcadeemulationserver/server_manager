

-- returns the player object of the closest player to <pos>, and the distance of the player
server_manager.closest_player = function(pos)

    local players = minetest.get_connected_players()
    local closest 
    local distance
    for _, player in pairs(players) do 
        if not closest then
            closest = player
            distance = math.abs(vector.length(vector.subtract(pos , player:get_pos())))
        else
            local pl_dist = math.abs(vector.length(vector.subtract(pos , player:get_pos())))
            if pl_dist < distance then
            closest = player
            distance = pl_dist
            end
        end
    end

    return closest , distance

end


--kills sheep inside range

server_manager.kill_sheep_in_radius = function( pos , radius )
    local objects = minetest.get_objects_inside_radius(pos, 0.5)
    for _, v in ipairs(objects) do
        if v:get_luaentity().name == "server_manager:sheep" then
            local luaent = v:get_luaentity()
            luaent.hp = 0
        end
    end
end


