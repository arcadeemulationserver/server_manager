# version: 1.1.1
# author(s):
# reviewer(s):
# textdomain: server_manager

# broadcast.lua
Got some ideas about how to improve the server? Go to the main square, write us a book and put it into the suggestion box!=
Why did we choose Minetest (which is an engine) instead of Minecraft (which is a videogame)? Because it's free software!=
Do you want to develop your own minigame? We made a library to help you out: search for 'arena_lib' on GitLab and... maybe one day we'll also feature it here!=
Do you want to support what we do (server and mods)? Offer us a coffee on liberapay.com/EticaDigitale=
Playing while in a call is way more fun than alone; and we have a Mumble server for that! Join us at 185.242.180.132=
Looking for people to play with or you want to follow the evolution of the server? Take part in our Matrix community: +minetest-aes:matrix.org (you need a client like Element)=

# nodes.lua
Barrier=
