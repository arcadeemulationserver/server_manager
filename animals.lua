-----------
-- Sheep --
-----------

local blend = better_fauna.frame_blend

local min = math.min
local abs = math.abs
local random = math.random

local function sheep_logic(self)

	if self.hp <= 0 then
		mob_core.on_die(self)
		return
	end

	local pos = mobkit.get_stand_pos(self)
	local prty = mobkit.get_queue_priority(self)
	local player = mobkit.get_nearby_player(self)

	mob_core.random_sound(self, 16/self.dtime)

	if mobkit.timer(self,0.5) then 

		mob_core.vitals(self)
		mob_core.growth(self)

		if self.status ~= "following" then
            if self.attention_span > 1 then
                self.attention_span = self.attention_span - 1
                mobkit.remember(self, "attention_span", self.attention_span)
            end
		else
			self.attention_span = self.attention_span + 1
			mobkit.remember(self, "attention_span", self.attention_span)
		end

        if prty < 2
        and player then
            better_fauna.hq_sporadic_flee(self, 10, player)
        end

		if mobkit.is_queue_empty_high(self) then
			mob_core.hq_roam(self, 0)
		end
	end
end

minetest.register_entity("server_manager:sheep",{
	max_hp = 20,
	view_range = 16,
	armor_groups = {fleshy = 100},
	physical = true,
	collide_with_objects = true,
	collisionbox = {-0.4, -0.4, -0.4, 0.4, 0.4, 0.4},
	visual_size = {x = 10, y = 10},
	scale_stage1 = 0.5,
    scale_stage2 = 0.65,
    scale_stage3 = 0.80,
	visual = "mesh",
	mesh = "better_fauna_sheep.b3d",
	textures = {"better_fauna_sheep.png^better_fauna_sheep_wool.png"},
	child_textures = {"better_fauna_sheep.png"},
	animation = {
		stand = {range = {x = 30, y = 50}, speed = 10, frame_blend = blend, loop = true},
		walk = {range = {x = 1, y = 20}, speed = 30, frame_blend = blend, loop = true},
		run = {range = {x = 1, y = 20}, speed = 45, frame_blend = blend, loop = true},
	},
    sounds = {
        alter_child_pitch = true,
        random = {
            name = "better_fauna_sheep_idle",
            gain = 1.0,
            distance = 8
        },
        hurt = {
            name = "better_fauna_sheep_idle",
			gain = 1.0,
			pitch = 0.5,
            distance = 8
        },
        death = {
            name = "better_fauna_sheep_idle",
			gain = 1.0,
			pitch = 0.25,
            distance = 8
        }
    },
	max_speed = 4,
	stepheight = 1.1,
	jump_height = 1.1,
	buoyancy = 0.25,
	lung_capacity = 10,
    timeout = 1200,
    ignore_liquidflag = false,
    core_growth = false,
	push_on_collide = true,

	consumable_nodes = {
		{
			name = "default:dirt_with_grass",
			replacement = "default:dirt"
		},
		{
			name = "default:dry_dirt_with_dry_grass",
			replacement = "default:dry_dirt"
		}
	},
	get_staticdata = mobkit.statfunc,
	logic = sheep_logic,
	on_step = function(self, dtime, moveresult)
		better_fauna.on_step(self, dtime, moveresult)
		if mobkit.is_alive(self) then
			if self.object:get_properties().textures[1] == "better_fauna_sheep.png"
			and not self.gotten then
				self.object:set_properties({
					textures = {"better_fauna_sheep.png^better_fauna_sheep_wool.png"},
				})
			end
		end
	end,
	on_activate = function(self, staticdata, dtime_s)
		better_fauna.on_activate(self, staticdata, dtime_s)
		self.dye_color = mobkit.recall(self, "dye_color") or "white"
		self.dye_hex = mobkit.recall(self, "dye_hex") or ""
		if self.dye_color ~= "white"
		and not self.gotten then
			self.object:set_properties({
				textures = {"better_fauna_sheep.png^(better_fauna_sheep_wool.png^[colorize:" .. self.dye_hex .. ")"},
			})
		end
		if self.gotten then
			self.object:set_properties({
				textures = {"better_fauna_sheep.png"},
			})
		end
	end,
	
	on_punch = function(self, puncher, _, tool_capabilities, dir)
		mob_core.on_punch_basic(self, puncher, tool_capabilities, dir)
		better_fauna.hq_sporadic_flee(self, 10, puncher)
	end,
})
