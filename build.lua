local function target_contains() end
local function utarget_contains() end

function minetest.is_protected(pos, name)
  
  if minetest.check_player_privs(name, {build = true}) then
    return false
  elseif utarget_contains( minetest.get_node( {x = pos.x , y = pos.y -1 , z = pos.z } ).name ) or utarget_contains( minetest.get_node( {x = pos.x , y = pos.y - 2 , z = pos.z } ).name ) then -- allows building over myboardgames
    return false

  else
    
    local player = minetest.get_player_by_name(name)
    if player then
      player:set_look_vertical(-1.57)
    end
    return true

  end
end

minetest.register_privilege("build", {
    description = "Can build",
    give_to_singleplayer = false,
    give_to_admin = true
})


function target_contains(str) 

  if string.find( str , "my"  )  
    and not ( string.find( str , "mypitfall") )
    and not ( string.find( str , ":block") ) 
    and not ( string.find( str , ":board") ) then

    return true

  else

    return false

  end

end



function utarget_contains(str) 

  if string.find( str , "my"  )  
    and not ( string.find( str , "mypitfall") ) then

    return true

  else

    return false

  end

end
